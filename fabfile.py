
import os

from fabric.api import env, run, sudo
from fabric.contrib.project import rsync_project

current_dir = os.path.dirname(os.path.abspath(__file__))
remote_dir = '/home/pi/raspgio'

env.hosts = ['raspberry']
env.use_ssh_config = True


def clean():
    run('rm -rf {}'.format(remote_dir))


def sync():
    rsync_project(
        local_dir=current_dir + '/',
        remote_dir=remote_dir,
        exclude='.git'
    )


def dependencies():
    sudo('apt-get update')
    sudo('apt-get install -qq python python-zmq python-protobuf python-pip')
    sudo('pip install RPi.GPIO')


def host_type():
    run('uname -s')
