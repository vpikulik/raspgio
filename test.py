import unittest
from mock import Mock, patch, call

from raspgio.server import GPIOServer
from raspgio.client import GPIOClient
import raspgio.messages_pb2 as msg


@patch('raspgio.server.GPIO')
@patch('raspgio.server.spidev')
class TestRaspgioServer(unittest.TestCase):

    def setUp(self):
        self.server = GPIOServer()

    def _get_request(self, action, pins=None):
        request = msg.Request()
        request.action = action

        if pins:
            for pin in pins:
                request.pins.append(pin)

        return request

    def test_cleanup(self, spi_mock, gpio_mock):
        request = self._get_request(msg.Request.CLEANUP)
        self.server.handle_request(request)
        self.assertTrue(gpio_mock.cleanup.called)

    def test_set_pins(self, spi_mock, gpio_mock):
        request = self._get_request(msg.Request.SET, pins=[1, 5])
        request.params.mode = msg.PinStatus.OUTPUT
        request.params.value = 1
        response = self.server.handle_request(request)
        self.assertEquals(
            gpio_mock.output.call_args_list,
            [
                call(1, 1),
                call(5, 1),
            ]
        )

        self.assertEquals(response.status, msg.Response.SUCCESS)
        self.assertEquals(response.pins[0].pin, 1)
        self.assertEquals(response.pins[0].params.mode, msg.PinStatus.OUTPUT)
        self.assertEquals(response.pins[0].params.value, 1)
        self.assertEquals(response.pins[1].pin, 5)
        self.assertEquals(response.pins[1].params.mode, msg.PinStatus.OUTPUT)
        self.assertEquals(response.pins[1].params.value, 1)

    def test_pwm(self, spi_mock, gpio_mock):
        request = self._get_request(msg.Request.SET, pins=[3])
        request.params.mode = msg.PinStatus.PWM
        request.params.frequency = 50
        request.params.duty_cycle = 30
        response = self.server.handle_request(request)
        self.assertEquals(
            gpio_mock.PWM.call_args_list,
            [call(3, 50)]
        )
        pwm_mock = gpio_mock.PWM.return_value
        pwm_mock.ChangeDutyCycle.assert_called_once_with(30)
        pwm_mock.start.assert_called_once_with()

        self.assertEquals(response.status, msg.Response.SUCCESS)
        self.assertEquals(response.pins[0].pin, 3)
        self.assertEquals(response.pins[0].params.mode, msg.PinStatus.PWM)
        self.assertEquals(response.pins[0].params.frequency, 50)
        self.assertEquals(response.pins[0].params.duty_cycle, 30)

    def test_spi_send(self, spi_mock, gpio_mock):
        request = self._get_request(msg.Request.SPI_SEND)
        request.spi.tx.extend([0xa, 0xb, 0xc])
        response = self.server.handle_request(request)

        spi_instance = spi_mock.SpiDev.return_value
        spi_instance.xfer.assert_called_once_with([0xa, 0xb, 0xc])

        self.assertEquals(response.status, msg.Response.SUCCESS)
        self.assertEquals(response.spi, request.spi)

    def test_spi_receive(self, spi_mock, gpio_mock):
        spi_instance = spi_mock.SpiDev.return_value
        spi_instance.readbytes.return_value = [1, 2, 3]

        request = self._get_request(msg.Request.SPI_RECEIVE)
        request.spi.expected_bytes = 3
        response = self.server.handle_request(request)

        spi_instance.readbytes.assert_called_once_with(3)
        self.assertEquals(response.status, msg.Response.SUCCESS)
        self.assertEquals(response.spi.rx, [1, 2, 3])


@patch('raspgio.client.GPIOClient.connect', Mock)
@patch('raspgio.client.GPIOClient._run')
class TestRaspgioClient(unittest.TestCase):

    def setUp(self):
        self.client = GPIOClient()

    def test_get_status(self, client_run_mock):
        self.client.get_status()
        request = client_run_mock.call_args[0][0]
        self.assertEquals(request.action, msg.Request.STATUS)

    def test_cleanup(self, client_run_mock):
        self.client.cleanup()
        request = client_run_mock.call_args[0][0]
        self.assertEquals(request.action, msg.Request.CLEANUP)

    def test_pin_output(self, client_run_mock):
        self.client.set_pin_output(7, 1)
        request = client_run_mock.call_args[0][0]
        self.assertEquals(request.action, msg.Request.SET)
        self.assertEquals(request.pins, [7])
        self.assertEquals(request.params.mode, msg.PinStatus.OUTPUT)
        self.assertEquals(request.params.value, 1)

    def test_pwm_output(self, client_run_mock):
        self.client.set_pin_pwm(8, 30, 50)
        request = client_run_mock.call_args[0][0]
        self.assertEquals(request.action, msg.Request.SET)
        self.assertEquals(request.pins, [8])
        self.assertEquals(request.params.mode, msg.PinStatus.PWM)
        self.assertEquals(request.params.frequency, 30)
        self.assertEquals(request.params.duty_cycle, 50)
