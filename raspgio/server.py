
from mock import Mock
try:
    import RPi.GPIO as GPIO
except ImportError:
    print 'GPIO was not imported!!!'
    GPIO = Mock()

try:
    import spidev
except ImportError:
    print 'SpiDev was not imported!!!'
    spidev = Mock()

import zmq

import messages_pb2 as msg


class GPIOServerException(Exception):
    pass


class GPIOServer(object):

    def __init__(self):
        self._pins = {}
        self._pwm = {}
        self._spi = None

        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REP)

    def _decode(self, message):
        request = msg.Request()
        request.ParseFromString(message)
        return request

    def _encode(self, body):
        return body.SerializeToString()

    def run(self):
        self.socket.bind("tcp://*:1510")
        while True:
            try:
                message = self.socket.recv()
                request = self._decode(message)
                response = self.handle_request(request)
                resp_message = self._encode(response)
                self.socket.send(resp_message)

            except GPIOServerException as exc:
                response = self.handle_exception(exc)
                resp_message = self._encode(response)
                self.socket.send(resp_message)

    def handle_request(self, request):
        if request.action == msg.Request.STATUS:
            pass
        elif request.action == msg.Request.SET:
            for pin in request.pins:
                self._set_pin(pin, request.params)
        elif request.action == msg.Request.CLEANUP:
            GPIO.cleanup()
        elif request.action == msg.Request.SPI_SEND:
            self._spi_send(request.spi)
        elif request.action == msg.Request.SPI_RECEIVE:
            self._spi_receive(request.spi)
        else:
            raise GPIOServerException('Wrong action')
        return self._get_response()

    def handle_exception(self, exc):
        response = msg.Response()
        response.status = msg.Response.SUCCESS
        response.error_msg = exc.message
        return response

    def _clean_pin(self, pin):
        params = self._pins.get(pin)
        if not params:
            return
        if params.mode == msg.PinStatus.OUTPUT:
            GPIO.output(pin, 0)
        elif params.mode == msg.PinStatus.PWM:
            if pin in self._pwm:
                self._pwm[pin].stop()
                del self._pwm[pin]
        del self._pin[pin]

    def _set_pin(self, pin, params):
        self._clean_pin(pin)
        self._pins[pin] = params

        if params.mode == msg.PinStatus.OUTPUT:
            if params.value not in (0, 1):
                raise GPIOServerException('Wrong value {} for pin {}'.format(params.value, pin))
            GPIO.output(pin, params.value)

        elif params.mode == msg.PinStatus.INPUT:
            raise GPIOServerException('Input mode is not supported yet')

        elif params.mode == msg.PinStatus.PWM:
            if not params.frequency:
                raise GPIOServerException('Wrong frequency {} for pin {}'.format(params.frequency, pin))
            if not (params.duty_cycle >= 0 and params.duty_cycle <= 100):
                raise GPIOServerException('Wrong duty cycle {} for pin {}'.format(params.duty_cycle, pin))
            pwm = GPIO.PWM(pin, params.frequency)
            pwm.ChangeDutyCycle(params.duty_cycle)
            pwm.start()
            self._pwm[pin] = pwm

    def _get_spi(self):
        spi = spidev.SpiDev()
        spi.open(0, 0)
        spi.max_speed_hz = 5000
        spi.mode = 0b10
        return spi

    def _spi_send(self, spi_status):
        self._spi = spi_status
        spi = self._get_spi()
        spi.xfer(spi_status.tx)
        spi.close()

    def _spi_receive(self, spi_status):
        self._spi = msg.SpiStatus()
        spi = self._get_spi()
        result = spi.readbytes(spi_status.expected_bytes)
        self._spi.rx.extend(result)
        spi.close()

    def _get_response(self):
        response = msg.Response()
        response.status = msg.Response.SUCCESS
        for pin, params in self._pins.items():
            item = response.pins.add()
            item.pin = pin
            item.params.CopyFrom(params)
        if self._spi:
            response.spi.CopyFrom(self._spi)
        return response
