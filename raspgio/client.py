
import zmq

import messages_pb2 as msg


class GPIOClient(object):

    MODE_MAP = {
        msg.PinStatus.OUTPUT: u'Output',
        msg.PinStatus.INPUT: u'Input',
        msg.PinStatus.PWM: u'Pwm',
    }

    STATUS_MAP = {
        msg.Response.SUCCESS: u'Success',
        msg.Response.ERROR: u'Error',
    }

    def __init__(self):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REQ)

    def connect(self):
        self.socket.connect("tcp://localhost:5555")

    def _send(self, message):
        self.socket.send(message)
        return self.socket.recv()

    def _encode(self, request):
        return request.SerializeToString()

    def _decode(self, answer):
        response = msg.Response()
        response.ParseFromString(answer)
        return response

    def _print(self, response):
        out = {'status': self.STATUS_MAP[response.status]}
        if response.status == msg.Response.response:
            out['pins'] = self._print_pins(response.pins)
        else:
            out['message'] = response.error_msg

    def _print_pins(self, pins):
        return [
            {
                'pin': pin_item.pin,
                'params': self._print_pin_status(pin_item.params),
            }
            for pin_item in pins
        ]

    def _print_pin_status(self, pin_status):
        out = {'mode': self.MODE_MAP[pin_status.mode]}
        if pin_status.mode in (msg.PinStatus.OUTPUT, msg.PinStatus.INPUT):
            out['value'] = pin_status.value
        elif pin_status.mode == msg.PinStatus.PWM:
            out.update({
                'frequency': pin_status.frequency,
                'duty_cicle': pin_status.duty_cicle,
            })
        return out

    def _run(self, request):
        message = self._encode(request)
        answer = self._send(message)
        response = self._decode(answer)
        return self._print(response)

    def _get_request(self, action, pins=None):
        request = msg.Request()
        request.action = action

        if pins:
            for pin in pins:
                request.pins.append(pin)

        return request

    def get_status(self):
        return self._run(self._get_request(msg.Request.STATUS))

    def cleanup(self):
        return self._run(self._get_request(msg.Request.CLEANUP))

    def set_pin_output(self, pin, value):
        request = self._get_request(msg.Request.SET, pins=[pin])
        request.params.mode = msg.PinStatus.OUTPUT
        request.params.value = 1
        return self._run(request)

    def set_pin_pwm(self, pin, frequency, duty_cycle):
        request = self._get_request(msg.Request.SET, pins=[pin])
        request.params.mode = msg.PinStatus.PWM
        request.params.frequency = frequency
        request.params.duty_cycle = duty_cycle
        return self._run(request)
