#!/usr/bin/env python

import IPython

from raspgio.client import GPIOClient


client = GPIOClient()
IPython.start_ipython(
    argv=[],
    user_ns={
        'cl': client,
    },
    banner1='Welcome to Raspgio client'
)
