
RASPBERRY_HOST = raspberry
FAB = fab -u pi -H $(RASPBERRY_HOST)

clean:
	find . -name '*.pyc' -delete
	rm -rf *_pb2.py

rclean:
	$(FAB) clean

messages_pb2.py:
	make -C ./raspgio messages_pb2.py

sync: messages_pb2.py
	$(FAB) sync

dependencies:
	$(FAB) dependencies

test: messages_pb2.py
	nosetests
